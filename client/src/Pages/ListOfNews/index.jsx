import React, { useEffect,
  useMemo,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Paginate from "react-paginate";

import { getData, logOut } from '../../store/actions';
import { LOG_IN_URL, REGISTRATION_URL, CLIENT_URL } from '../../store/constants'
import { News } from '../News';

export default function ListOfNews() {
  const pagination = { 
    amountNewsOnPage: 5,
  }

  let arrayOfNews = useSelector(state => state ? state.news: []);

  const dispatch = useDispatch();
  const [filterFlag, changeFilterFlag] = useState('all');
  const [arrayForRender, changeArrayForRender] = useState(JSON.parse(JSON.stringify(arrayOfNews)))
  const [filterArray, changeFilterArray] = useState(JSON.parse(JSON.stringify(arrayOfNews)))
  const [pageCount, changePageCount] = useState(0);
  const [currentPage, changeCurrentPage] = useState(0);

  useEffect(() => {
    if(!arrayOfNews.length)
    dispatch(getData());
  }, [])

  useEffect(() => {
    changeFilterArray(JSON.parse(JSON.stringify(arrayOfNews)))
  }, [arrayOfNews])

  useEffect(() => {
    changeArrayForRender(JSON.parse(JSON.stringify(filterArray)).slice(0, pagination.amountNewsOnPage))
    changePageCount(Math.ceil(filterArray.length / pagination.amountNewsOnPage))
  }, [filterArray])

  useEffect(() => {
    const startOfRender = pagination.amountNewsOnPage*currentPage
    changeArrayForRender(filterArray.slice(startOfRender, startOfRender + pagination.amountNewsOnPage))
  }, [currentPage])

  const splittingArrayOfNews = useMemo(() =>
  arrayForRender.map((news) => 
    <News 
      title={news.title}
      image={news.image || 'default_news.jpg'}
      describe={news.describe}
      key={news.id}
      tags={news.tags}
      author={news.news_user_id.login}
    />
  ), [arrayForRender])

const handleChangeSearchingField = (event) => {
  changeFilterArray(() => {
    switch (filterFlag) {
      case 'all':
        return arrayOfNews.filter((news) => {
          return news.title.includes(event.target.value) ||
                 news.describe.includes(event.target.value) ||
                 news.tags.includes(event.target.value) ||
                 news.news_user_id.login.includes(event.target.value)
        })
      case 'tags':
        return arrayOfNews.filter((news) => news.tags.includes(event.target.value))
      case 'author':
        return arrayOfNews.filter((news) => news.news_user_id.login.includes(event.target.value))
      default:
        return arrayOfNews;
    }
  })
};

const handleChangeFilter = (event) => {
  changeFilterFlag(event.target.value);
}

const handlePageClick = (event) => {
  changeCurrentPage(event.selected)
}

const handleClickOnLogOut = () => {
  localStorage.setItem('token', '');
  localStorage.setItem('google', '')
  dispatch(logOut())
}

const handleClickOnHomePage = (e) => {
  e.preventDefault();
    window.location = `${CLIENT_URL}/home?owner=true`
}

  return (
    <div className="wrapper">
      <div className='header'>
          {(localStorage.getItem('token')) ?
            (
              <div>
                <a className="link" href={CLIENT_URL} onClick={handleClickOnLogOut} >Log Out</a>
                <a className="link" href={CLIENT_URL} onClick={handleClickOnHomePage} >Home</a>
              </div>
            )
            :
            (
              <div>
                <a className="link" href={LOG_IN_URL}>Log In</a>
                <a className="link" href={REGISTRATION_URL}>Register</a>
              </div>
            )
          }
      </div>
      
      <div className="news-and-paginate">
        <div className='searching-field'>
          <input type='text' className = "news-page__searching-field" onChange={handleChangeSearchingField} ></input>
          <select onChange = {handleChangeFilter} >
            <option value='all'>all</option>
            <option value='tags'>tags</option>
            <option value='author'>author</option>
          </select>
        </div>
        <div className="list-of-news">
        {
          arrayOfNews.length ?
          (
            splittingArrayOfNews
          )
          : (<div>Нет постов</div>)
        }
        </div>
        <div className="paginator main-page-paginator">
        <Paginate
          previousLabel={"prev"}
          nextLabel={"next"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={pagination.amountNewsOnPage}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
        />
        </div>
      </div>
    </div>
  );
}
