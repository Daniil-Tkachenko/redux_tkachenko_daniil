import React, {
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Paginate from "react-paginate";

import { getNewsOfUser, getUserFromDatabase, getUser } from '../../store/actions';
import { CLIENT_URL } from "../../store/constants";
import { News } from '../News';
import { ModalAddNews } from '../ModalAddNews';
import { ModalEditNews } from '../ModalEditNews';


function UserPage() {

    const pagination = { 
        amountNewsOnPage: 5,
      }
    
    const dispatch = useDispatch();

    const [arrayOfNews, changeArrayOfNews] = useState([]);
    const news = useSelector(state => state ? state.news: [])

    const searchParams = new URLSearchParams(window.location.search);
    const isOwner = Boolean(searchParams.get("owner"))
    const googleUser = localStorage.getItem('google')

    let { id: id_author } = isOwner ? JSON.parse(localStorage.getItem('user')) : JSON.parse(localStorage.getItem('guest'))

    const user = useSelector(state => state ? state.user: {})
    const [userName, changeUserName] = useState('');
    const [avatar, changeAvatar] = useState('');

    useEffect(() => {
        dispatch(getUser(id_author))
    }, [])

    useEffect(() => {
        if(Object.keys(user).length) {
            changeUserName(user.login)
            changeAvatar(user.avatar)
        }
    }, [user])

    useEffect(() => {
        if(news) {
            changeArrayOfNews(news);
        }
    }, [news])

    const [arrayForRender, changeArrayForRender] = useState(JSON.parse(JSON.stringify(arrayOfNews)))
    const [currentPage, changeCurrentPage] = useState(0);
    const [showModalAddNews, changeShowModalAddNews] = useState(false);
    const [showModalEditNews, changeShowModalEditNews] = useState(false);
    const URLImage = 'default_avatar.png';

    useEffect(() => {
        if(userName) {
            dispatch(getNewsOfUser(id_author));
        }
    }, [userName])

    useEffect(() => {
        if(arrayOfNews.length) {
            changeArrayForRender(arrayOfNews.slice(currentPage*pagination.amountNewsOnPage, currentPage*pagination.amountNewsOnPage + pagination.amountNewsOnPage))
        }
    }, [arrayOfNews])

    useEffect(() => {
        const startOfRender = pagination.amountNewsOnPage*currentPage
        changeArrayForRender(arrayOfNews.slice(startOfRender, startOfRender + pagination.amountNewsOnPage))
    }, [currentPage])

    const handleClickAddNews = () => {
        changeShowModalAddNews(true);
    }

    const handleClickEditNews = () => {
        changeShowModalEditNews(true);
    }

    const renderArrayOfNews = useMemo(() =>
    arrayForRender.map((news) => 
      <News
        title={news.title}
        image={news.image || 'default_news.jpg'}
        describe={news.describe}
        key={news.id}
        tags={news.tags}
        author={news.news_user_id.login}
      />
    ), [arrayForRender])

    const handleClickAcceptAddNews = (news) => {
        changeShowModalAddNews(false);
    }

    const handleClickAcceptEditUser = (user) => {
        changeShowModalEditNews(false);
        dispatch(getUser(id_author))
    }

    const handlePageClick = (event) => {
        changeCurrentPage(event.selected)
      }

    const pageCount = Math.ceil(arrayOfNews.length / pagination.amountNewsOnPage)

    const handleClickOnLogOut = () => {
        localStorage.setItem('token', '');
        localStorage.setItem('google', '')
        dispatch(logOut())
        window.location = `${CLIENT_URL}`
      }
    
    const closeEditModal = () => {
        changeShowModalEditNews(false)
    }

    const closeAddModal = () => {
        changeShowModalAddNews(false)
    }

    return (
        <div>
            <div className='user-page'>
                <div className="header">
                    <a className="link" href={CLIENT_URL} >Main page</a>
                    <a className="link" href={CLIENT_URL} onClick={handleClickOnLogOut} >Log Out</a>
                </div>
                <div className="user-info">
                    <h2>Login: <span>{ userName }</span></h2>
                    <img src={avatar || URLImage} className='user-avatar'></img>
                    { isOwner &&
                        (
                            <div>
                                <input type='button' value='Add News' onClick={handleClickAddNews}></input>
                                {
                                    ( !googleUser &&
                                        <input type='button' value='Edit User' onClick={handleClickEditNews} ></input>
                                    )
                                }
                            </div>
                        )
                    }
                </div>

                <div className='list-of-news'>
                    {renderArrayOfNews}
                </div>
                <Paginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={pagination.amountNewsOnPage}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                />
            </div>
            { showModalAddNews &&
                    (
                    <ModalAddNews
                        handleClickAccept = {handleClickAcceptAddNews}
                        id_author = {id_author}
                        closeAddModal = {closeAddModal}
                    />
                    )
                }
                { showModalEditNews &&
                    (
                    <ModalEditNews
                        handleClickAccept = {handleClickAcceptEditUser}
                        
                        id_author = {id_author}
                        login = {userName}
                        closeEditModal = {closeEditModal}
                    />
                    )
                }
        </div>
    )
}

export const User = React.memo(UserPage);