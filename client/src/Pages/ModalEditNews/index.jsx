import { useState } from 'react';
import { useDispatch } from 'react-redux';

import { editUserInDatabase, sendImageToDatabase } from '../../store/actions';
import { validateLoginCredentials } from '../../utils';

export function ModalEditNews(props) {

    const dispatch = useDispatch();
    const [login, changeLogin] = useState(props.login);
    const [password, changePassword] = useState(props.login);

    const handleClickAccept = () => {
        const { id_author } = props

        const user = {
            email: login,
            password: password,
            id_author: id_author,
        }

        if(validateLoginCredentials(login, password)) {
            dispatch(editUserInDatabase(user))
            props.handleClickAccept(user);
        } else {
            alert("Введены некорректные данные!")
        }
    }

    const uploadAvatarInDatabase = (event) => {
        event.preventDefault();
        let imageData = document.querySelector('input[type="file"]').files[0];
        const { id_author } = props

        let image = new FormData();
        image.append("image", imageData, imageData.name);

        const data = {
            image: image,
            id: id_author,
        }

        dispatch(sendImageToDatabase(data))
    }

    const handleClickCancel = () => {
        props.closeEditModal();
    }

    return (
        <div className="modal">
            <p>Login</p>
            <p><input className='edit-field' type='text' value={login} onChange={(event) => {changeLogin(event.target.value)}} ></input></p>
            <p>Password</p>
            <p><input className='edit-field' type='text' onChange={(event) => {changePassword(event.target.value)}} ></input></p>
            <p>User's avatar</p>
            <form id="formElem" encType="multipart/form-data" onChange={uploadAvatarInDatabase} >
                <input type="file" id="profile_pic" name="image" accept=".jpg, .jpeg, .png" ></input>
            </form>
            <p><input className='edit-accept' type='button' value='Accept' onClick={handleClickAccept} ></input></p>
            <p><input className='edit-accept' type='button' value='Cancel' onClick={handleClickCancel} ></input></p>
        </div>
    )
}