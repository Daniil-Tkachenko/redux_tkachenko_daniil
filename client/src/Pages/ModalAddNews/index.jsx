import { useState } from 'react';
import { useDispatch } from 'react-redux';

import { addNewsInDatabase } from '../../store/actions';

export function ModalAddNews(props) {

    const dispatch = useDispatch();
    const [title, changeTitle] = useState('');
    const [describe, changeDescribe] = useState('');
    const [tags, changeTags] = useState('');

    const handleClickAccept = () => {

        const imageNode = document.querySelector('input[type="file"]')
        let image;
        if(imageNode.files[0]) {
            let imageData = imageNode.files[0];
            image = new FormData();
            image.append("image", imageData, imageData.name);
        }

        const { id_author } = props
        const news = {
            title: title,
            describe: describe,
            image: image || null,
            id_author: id_author,
            tags: tags
        }

        if (title && describe && tags && title.trim() && describe.trim() && tags.trim()) {
            dispatch(addNewsInDatabase(news))
            props.handleClickAccept(news);
        } else {
            alert("Не все поля заполнены")
        }
    }

    const handleClickCancel = () => {
        props.closeAddModal();
    }

    return (
        <div className="modal">
            <p>title</p>
            <p className="title-input" ><input className='edit-field' type='text' onChange={(event) => {changeTitle(event.target.value)}} ></input></p>
            <p>Image</p>
            <form id="formElem" encType="multipart/form-data" >
                <input type="file" name="image" accept=".jpg, .jpeg, .png" ></input>
            </form>
            <p>describe</p>
            <input className='edit-field' type='text' onChange={(event) => {changeDescribe(event.target.value)}} ></input>
            <p>tags</p>
            <input className='edit-field' type='text' onChange={(event) => {changeTags(event.target.value)}} ></input>
            <p><input className='edit-accept' type='button' value='Accept' onClick={handleClickAccept} ></input></p>
            <p><input className='edit-accept' type='button' value='Cancel' onClick={handleClickCancel} ></input></p>
        </div>
    )
}