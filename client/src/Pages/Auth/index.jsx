import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

import { loadToken, register, googleAuthAction } from '../../store/actions'
import { CLIENT_URL, GOOGLE_CLIENT_ID } from '../../store/constants'

function Auth(props) {
    const [login, changeLogin] = useState('');
    const [password, changePassword] = useState('');

    const dispatch = useDispatch();

    const handleChangeLoginField = (event) => {
        changeLogin(event.target.value);
    }

    const handleChangePasswordField = (event) => {
        changePassword(event.target.value);
    }

    const handleClickOnAccept = () => {
        const data = {
            login: login,
            password: password,
        }
        if(login && password) {
            dispatch(loadToken(data));
        } else {
            alert("Пожалуйста, заполните все поля")
        }
    }

    function validate(login) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var address = login;
        if(reg.test(address) == false) {
           return false;
        }
        return true;
     }

    const checkRegister = (login, password) => {
        return login && password && login.trim() && password.trim() && validate(login) ? true : false
    }

    const handleClickOnRegister = () => {
        const data = {
            login: login,
            password: password,
        }
        if(checkRegister(login, password)) {
            dispatch(register(data));
        } else {
            alert("Введены некорректные данные!")
        }
    }

    const scriptLoaded = () => {
        gapi.load('auth2', function() {
            gapi.auth2.init({
                client_id: GOOGLE_CLIENT_ID,
            })
          });
    }

    useEffect(() => {
        const script = document.createElement("script");
        script.src = "https://apis.google.com/js/platform.js";
        script.async = true;
        script.onload = () => scriptLoaded();
      
        document.body.appendChild(script);
    }, [])

    const signInGoogle = () => {
        const googleAuth = gapi.auth2.getAuthInstance()
        googleAuth.signIn({
            scope: 'profile email'
        })
            .then((user) => {
                const googleLogin = user.getBasicProfile().getEmail();
                const googleAvatar = user.getBasicProfile().getImageUrl();
                const data = {
                    email: googleLogin,
                    avatar: googleAvatar,
                }
                dispatch(googleAuthAction(data))
            })
    }

    return (
        <div className="auth-page">
            <a className="main-page-link" href={CLIENT_URL}>Main Page</a>
            <div className="auth-block">
                <p>Login</p>
                <input type='text' onChange={handleChangeLoginField} ></input>
                <p>Password</p>
                <p><input type='text' onChange={handleChangePasswordField} ></input></p>
                {
                    props.location.pathname === '/logIn/' ?
                    (
                        <input type='button' value="signIn" onClick={handleClickOnAccept} ></input>
                    )
                    :
                    (
                        <input type='button' value="Register" onClick={handleClickOnRegister} ></input>
                    )
                }

                <input type='button' onClick={signInGoogle} value="Log In Google" ></input>

            </div>
        </div>
    );
}

export default Auth;