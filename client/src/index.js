import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { reducer } from './store/reducers';
import { watcherLoadNews } from './store/sagas'

  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(reducer, compose(
    applyMiddleware(sagaMiddleware),
  ))

  sagaMiddleware.run(watcherLoadNews)

  const app =   <Provider store={store}>
                  <App />
                </Provider>
ReactDOM.render(
  app,
  document.getElementById('root')
);

reportWebVitals();
