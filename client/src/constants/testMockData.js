export const state = {
  news: [
    {
      createdAt: "2021-07-20T13:01:11.255Z",
      describe: "asd",
      id: 1,
      id_author: 1,
      image: null,
      news_user_id: {id: 1, login: "d.tkachenko@dunice.net", password: null, avatar: "https://lh3.googleusercontent.com/a-/AOh14GhfICNpuvr84UVZ78T201uVqqyUL64fxCUh5-3d=s96-c", createdAt: "2021-07-20T13:01:05.135Z" },
      tags: "das",
      title: "asd",
      updatedAt: "2021-07-20T13:01:11.255Z",
    },
    {
      createdAt: "2021-07-20T13:01:11.255Z",
      describe: "dsa",
      id: 2,
      id_author: 1,
      image: null,
      news_user_id: {id: 1, login: "d.tkachenko@dunice.net", password: null, avatar: "https://lh3.googleusercontent.com/a-/AOh14GhfICNpuvr84UVZ78T201uVqqyUL64fxCUh5-3d=s96-c", createdAt: "2021-07-20T13:01:05.135Z" },
      tags: "dsa",
      title: "das",
      updatedAt: "2021-07-20T13:01:11.255Z",
    }
  ],
};

export const newsProps = {
  describe: "asd",
  image: null,
  tags: "das",
  title: "asd",
  author: 'daniil',
}

export const emails = {
  wrongEmails: [
    '312123fssdf',
    '@gmail.com',
    'ya@',
    'anton@gmail',
    '',
    '    ',
  ],
  correctEmails: [
    'dtkachenko41@gmil.com',
    'd.tkachenko@dunice.net',
    'kopatich@mail.ru',
  ],
};

export const passwords = {
  wrongPasswords: [
    '',
    '     ',
  ],
  correctPasswords: [
    '123456',
    'qwerty',
  ],
};
