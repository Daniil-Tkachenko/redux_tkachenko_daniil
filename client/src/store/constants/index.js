export const GET_DATA = "GET_DATA"
export const PUT_DATA = "PUT_DATA"
export const LOAD_TOKEN = "LOAD_TOKEN"
export const PUT_TOKEN = "PUT_TOKEN"
export const REGISTER = "REGISTER"
export const LOG_OUT = "LOG_OUT"
export const GET_NEWS_OF_USER = "GET_NEWS_OF_USER"
export const PUT_NEWS_OF_USER = "PUT_NEWS_OF_USER"
export const ADD_NEWS = "ADD_NEWS"
export const EDIT_NEWS = "EDIT_NEWS"

export const PUT_USER = "PUT_USER"
export const GET_USER = "GET_USER"

export const REDIRECT_TO_USER_PAGE = "REDIRECT_TO_USER_PAGE"

export const SEND_IMAGE = "SEND_IMAGE"
export const SEND_IMAGE_NEWS = "SEND_IMAGE_NEWS"

export const SERVER_URL = "http://localhost:3000"
export const CLIENT_URL = "http://localhost:3001"

export const LOG_IN_URL = `${CLIENT_URL}/logIn/`
export const LOG_OUT_URL = `${CLIENT_URL}/logOut/`
export const REGISTRATION_URL = `${CLIENT_URL}/registration/`

export const GOOGLE_CLIENT_ID = '571769384230-hr2drqcjfno6n100ha5vghdrlo8op7mb.apps.googleusercontent.com'
export const GOOGLE_AUTH = "GOOGLE_AUTH"