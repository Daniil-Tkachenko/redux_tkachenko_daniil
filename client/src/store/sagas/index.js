import { call, takeEvery, put } from 'redux-saga/effects';
import axios from 'axios';

import { 
    GET_DATA,
    SERVER_URL,
    LOAD_TOKEN,
    REGISTER,
    GET_NEWS_OF_USER,
    ADD_NEWS, 
    EDIT_NEWS,
    SEND_IMAGE,
    PUT_USER,
    CLIENT_URL,
    SEND_IMAGE_NEWS,
    GOOGLE_AUTH,
    GET_USER,
    REDIRECT_TO_USER_PAGE,
} from '../constants';

import {
  putData,
  putToken,
  register,
  putNewsOfUser,
  sendNewsImage,
  getNewsOfUser,
  putUser,
  sendImageToDatabase,
} from '../actions';

function* workerLoadNews() {
    try {
        const news = yield call(axios, `${SERVER_URL}/news`, 
                                        { method: 'GET'});
        yield put(putData(news.data))
    } catch (e) {
        alert('Error!')
    }
}

function getUser(id_author) {
    return call(axios, `${SERVER_URL}/users/${id_author}`, {method: 'GET'})
}

function* workerSignIn(action) {

    let raw = {
      email: action.payload.login,
      password: action.payload.password,
    };

    raw = JSON.stringify(raw)

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      data: raw,
    };

    const url = action.type === 'LOAD_TOKEN'
        ? "/signin"
        : "/registration"

    try {

        const { data } = yield call(axios, `${SERVER_URL}${url}`, requestOptions )
        const { token, id_author, login } = data
        localStorage.setItem('token', token)
        yield put(putToken(token))

        const { data: user } = yield getUser(id_author)
        localStorage.setItem('user', JSON.stringify(user))

        localStorage.setItem('google', '')

        window.location = `${CLIENT_URL}/home?owner=true`

    } catch(error) {
        action.type === 'LOAD_TOKEN'
        ?   alert('Введены неверные учетные данные')
        :   alert('Пользователь с таким ником уже существует!')
        console.log(error)
    }
}

function* workerSendImageToDatabase(action) {

    const { id, image } = action.payload;

    const requestOptions = {
        method: 'POST',
        data: image,
      };

    try {
        const { data: user } = yield call(axios, `${SERVER_URL}/upload/${id}`, requestOptions)
    } catch {
        alert("Error!")
    }
}

function* workerGetNewsOfUser(action) {

    let raw = {
        id: action.payload.id
    }

    raw = JSON.stringify(raw)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        data: raw,
      };

    try {
        const { data: news } = yield call(axios, `${SERVER_URL}/users`, requestOptions)
        yield put(putNewsOfUser(news))
    } catch {
        alert("Error!")
    }
}

function* workerAddNewsInDatabase(action) {

    const { title, image, describe, tags, id_author } = action.payload;
    let raw = {
        title: title,
        image: null,
        describe: describe,
        tags: tags,
        id_author: id_author,
    }

    raw = JSON.stringify(raw)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        data: raw,
      };

    try {
        const { data: news } = yield call(axios, `${SERVER_URL}/news`, requestOptions)

        if (image) {
            const { id: id_news } = news
            const data = {
                image: image,
                id: id_news,
                id_author: id_author,
            }
            yield put(sendNewsImage(data))
        }
        yield put(getNewsOfUser(id_author))
    } catch {
        alert("Error!")
    }
}

function* workerSendNewsImageToDatabase(action) {

    const { id, image, id_author } = action.payload;

    const requestOptions = {
        method: 'POST',
        data: image,
      };

    try {
        const { data: user } = yield call(axios, `${SERVER_URL}/newsImage/${id}`, requestOptions)
        yield put(getNewsOfUser(id_author))
    } catch {
        alert("Error!")
    }
}

function* workerGoogleSignIn(action) {
    const { email, avatar } = action.payload
    let raw = {
      email: email,
    };

    raw = JSON.stringify(raw)

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      data: raw,
    };

    const url = '/googleAuth'

    try {
        axios(`${SERVER_URL}${url}`, requestOptions)
        .then((user) => {
            const { data } = user;
            const { id } = data;

            const args = {
                avatar: avatar,
                id_author: id,
            }
            axios(`${SERVER_URL}${url}/${id}`, {
                method: "PUT",
                headers: { 'Content-Type': 'application/json' },
                data: args,
            })
            .then(() => {
                localStorage.setItem('token', JSON.stringify("huba-buba"))
                localStorage.setItem('google', JSON.stringify("vse ok"))
                localStorage.setItem('user', JSON.stringify(data))
                window.location = `${CLIENT_URL}/home?owner=true`
            })
        })

    } catch(error) {
        alert('Error!')
        console.log(error)
    }
}

function* workerEditUserInDatabase(action) {

    let raw = {
        email: action.payload.email,
        id_author: action.payload.id_author,
        password: action.payload.password,
    }

    raw = JSON.stringify(raw)

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        data: raw,
      };

    try {
        const { data: user } = yield call(axios, `${SERVER_URL}/users`, requestOptions)
    } catch {
        alert("Error!")
    }
}

function* workerUpdateUserInDatabase(action) {
    
    const id_author = action.payload

    try {
        const { data: user } = yield getUser(id_author)
        const { id, login, avatar } = user
        const updatedUser = {
            id: id,
            login: login,
            avatar: avatar
        }
        yield put(putUser(updatedUser));
    } catch {
        alert("Error!!")
    }
}

function* workerRedirectToUserPage(action) {
    
    const login = action.payload

    try {
        const { data: user } = yield call(axios, `${SERVER_URL}/users/${login}`, {method: 'PATCH'})
        localStorage.setItem('guest', JSON.stringify(user))
        window.location = `${CLIENT_URL}/home?owner=`
        
    } catch {
        alert("Error!!!")
    }
}

export function* watcherLoadNews() {
    yield takeEvery(GET_DATA, workerLoadNews)
    yield takeEvery(LOAD_TOKEN, workerSignIn)
    yield takeEvery(REGISTER, workerSignIn)
    yield takeEvery(GET_NEWS_OF_USER, workerGetNewsOfUser)
    yield takeEvery(ADD_NEWS, workerAddNewsInDatabase)
    yield takeEvery(EDIT_NEWS, workerEditUserInDatabase)
    yield takeEvery(SEND_IMAGE, workerSendImageToDatabase)
    yield takeEvery(SEND_IMAGE_NEWS, workerSendNewsImageToDatabase)
    yield takeEvery(GOOGLE_AUTH, workerGoogleSignIn)
    yield takeEvery(GET_USER, workerUpdateUserInDatabase)
    yield takeEvery(REDIRECT_TO_USER_PAGE, workerRedirectToUserPage)
}
