import { 
    GET_DATA,
    PUT_DATA,
    LOAD_TOKEN,
    PUT_TOKEN,
    LOG_OUT,
    REGISTER,
    PUT_NEWS_OF_USER,
    PUT_USER,
    GET_USER,
} from '../constants';

const initialState = {
    news: [],
    token: '',
    user: {},
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DATA:
            return {
                ...state
            }
        case PUT_DATA:
            return {
                ...state, news: action.payload
            }
        case PUT_TOKEN:
            return {
                ...state, token: action.payload
            }
        case LOG_OUT:
            return {
                ...state, token: ''
            }
        case PUT_NEWS_OF_USER:
            return {
                ...state, news:action.payload
            }
        case PUT_USER:
            return {
                ...state, user:action.payload
            }
        }

}