import { 
    GET_DATA,
    PUT_DATA,
    LOAD_TOKEN,
    PUT_TOKEN,
    REGISTER,
    LOG_OUT,
    GET_NEWS_OF_USER,
    PUT_NEWS_OF_USER,
    ADD_NEWS,
    EDIT_NEWS,
    SEND_IMAGE,
    PUT_USER,
    SEND_IMAGE_NEWS,
    GOOGLE_AUTH,
    GET_USER,
    REDIRECT_TO_USER_PAGE,
} from '../constants'

export const getData = () => {
    return {
        type: GET_DATA,
        payload: null,
    }
}

export const putData = (data) => {
    return {
        type: PUT_DATA,
        payload: data,
    }
}

export const loadToken = (data) => {
    return {
        type: LOAD_TOKEN,
        payload: data,
    }
}

export const putToken = (data) => {
    return {
        type: PUT_TOKEN,
        payload: data,
    }
}

export const register = (data) => {
    return {
        type: REGISTER,
        payload: data,
    }
}

export const logOut = () => {
    return {
        type: LOG_OUT,
        payload: {},
    }
}

export const getNewsOfUser = (id) => {
    return {
        type: GET_NEWS_OF_USER,
        payload: {id},
    }
}

export const putNewsOfUser = (news) => {
    return {
        type: PUT_NEWS_OF_USER,
        payload: news,
    }
}

export const addNewsInDatabase = (news) => {
    return {
        type: ADD_NEWS,
        payload: news,
    }
}

export const editUserInDatabase = (data) => {
    return {
        type: EDIT_NEWS,
        payload: data,
    }
}

export const sendImageToDatabase = (data) => {
    return {
        type: SEND_IMAGE,
        payload: data,
    }
}

export const putUser = (data) => {
    return {
        type: PUT_USER,
        payload: data,
    }
}

export const getUser = (data) => {
    return {
        type: GET_USER,
        payload: data,
    }
}

export const sendNewsImage = (data) => {
    return {
        type: SEND_IMAGE_NEWS,
        payload: data,
    }
}

export const googleAuthAction = (data) => {
    return {
        type: GOOGLE_AUTH,
        payload: data,
    }
}

export const redirectToUserPageAction = (login) => {
    return {
        type: REDIRECT_TO_USER_PAGE,
        payload: login,
    }
}
