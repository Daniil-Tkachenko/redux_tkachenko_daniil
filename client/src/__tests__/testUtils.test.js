import { validate, validateLoginCredentials } from '../utils';
import { emails, passwords } from '../constants/testMockData';

describe('test utils functions', () => {
  let wrongEmails, correctEmails;
  let wrongPasswords, correctPasswords;
  beforeEach(() => {
    wrongEmails = [...emails.wrongEmails];
    correctEmails = [...emails.correctEmails];

    wrongPasswords = [...passwords.wrongPasswords];
    correctPasswords = [...passwords.correctPasswords];
  });

  test('test validate function', () => {
    wrongEmails.forEach((email) => {
      expect(validate(email)).toBe(false);
    });
    correctEmails.forEach((email) => {
      expect(validate(email)).toBe(true);
    });
  });
  test('test validateLoginCredentials function', () => {
    wrongPasswords.forEach((wrongPassword, index) => {
      expect(validateLoginCredentials(wrongEmails[index], wrongPassword)).toBe(false);
    });
    wrongPasswords.forEach((wrongPassword, index) => {
      expect(validateLoginCredentials(correctEmails[index], wrongPassword)).toBe(false);
    });
    correctPasswords.forEach((correctPassword, index) => {
      expect(validateLoginCredentials(wrongEmails[index], correctPassword)).toBe(false);
    });
    correctPasswords.forEach((correctPassword, index) => {
      expect(validateLoginCredentials(correctEmails[index], correctPassword)).toBe(true);
    });
  });
})