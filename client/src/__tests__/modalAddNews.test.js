import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { state } from '../constants/testMockData';
import { ModalAddNews } from '../Pages/ModalAddNews/index';

describe('tests for component ModalAddNews', () => {
  const mockStore = configureStore();
  let store;

  const handleClickAccept = jest.fn((data) => data);

  beforeEach(() => {
    store = mockStore(state);
    render(
      <Provider store={store}>
        <ModalAddNews
          id_author={2}
          handleClickAccept={handleClickAccept}
        />
      </Provider>
    );
  });


  test('creating news works right', () => {
    const wrapper = document.querySelector('.modal');
    expect(wrapper).toBeInTheDocument();

    const [titleField, describeField, tagsField] = document.querySelectorAll('.edit-field');
    const acceptButton = document.querySelector('.edit-accept');

    userEvent.type(titleField, 'title');
    userEvent.type(describeField, 'describe');
    userEvent.type(tagsField, 'tags');
    userEvent.click(acceptButton);

    expect(handleClickAccept).toHaveBeenCalled();
    expect(handleClickAccept).toHaveBeenCalledWith({
      title: 'title',
      describe: 'describe',
      image: null,
      id_author: 2,
      tags: 'tags'
    });
  });
});