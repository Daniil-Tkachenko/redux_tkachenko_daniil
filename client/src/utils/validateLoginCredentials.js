import { validate } from './validate';

export const validateLoginCredentials = (email, password) => {
  return email && password && email.trim() && password.trim() && validate(email) ? true : false
}