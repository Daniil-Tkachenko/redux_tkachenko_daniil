export const validate = (login) => {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var address = login;
  if (reg.test(address) == false) {
    return false;
  }
  return true;
}