import ListOfNews from './Pages/ListOfNews';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Auth from './Pages/Auth';
import { User } from './Pages/User'

function App() {
  return (
    <Router>
      <Switch>
          <Route exact path="/" component={ListOfNews} />
          <Route exact path='/logIn/' component={Auth}/>
          <Route exact path='/registration/' component={Auth}/>
          <Route path='/home' component={User}/>
      </Switch>
    </Router>
  );
}

export default App;
