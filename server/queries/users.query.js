const { User } = require('../models');

module.exports = {
  getUser(data) {
    return User.findOne({ where: data });
},
}