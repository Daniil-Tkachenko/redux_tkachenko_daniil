const { User } = require('../models');

module.exports = {
  getUserbyLogin(email) {
    return User.findOne({ where: { login: email } });
  },
  createUser({ email, password }) {
    return User.create({
      login: email,
      password: password,
    })
  },
};