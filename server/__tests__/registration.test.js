const { registration } = require('../controllers/registration');
const { registrationRequestObj } = require('../constants/testMockData');

describe('testing registration controller', () => {
  let res;
  let sendMock;
  let statusMock;
  let req;

  beforeEach(() => {
    res = {};
    sendMock = jest.fn(({ message }) => message);
    statusMock = jest.fn(status => {
      return {
        send: sendMock,
      }
    });
    res.status = statusMock;
  
    req = registrationRequestObj;
  });

  test('test regixtration with existing user credentials', async () => {
    jest.mock('../queries/registration.query', () => {
      return {
        getUserbyLogin: (email) => {
          return new Promise((res, rej) => {
            res({
              createdAt: "2021-07-20T13:01:11.255Z",
              describe: "dsa",
              id: 2,
              id_author: 1,
              image: null,
              news_user_id: {id: 1, login: "d.tkachenko@dunice.net", password: null, avatar: "https://lh3.googleusercontent.com/a-/AOh14GhfICNpuvr84UVZ78T201uVqqyUL64fxCUh5-3d=s96-c", createdAt: "2021-07-20T13:01:05.135Z" },
              tags: "dsa",
              title: "das",
              updatedAt: "2021-07-20T13:01:11.255Z",
            })
          });
        },
      }
    });

    await registration(req, res);
    expect(statusMock).toHaveBeenCalledWith(401);
    expect(sendMock).toHaveBeenCalledWith({ message: 'Такой пользователь уже существует' });
  });

  test('test regixtration with new user credentials', async () => {
    jest.mock('../queries/registration.query', () => {
      return {
        getUserbyLogin: (email) => {
          return new Promise((res, rej) => {
            res(null)
          });
        },
        createUser: ({ email, password }) => {
          return new Promise((res, rej) => {
            res({
              createdAt: "2021-07-20T13:01:11.255Z",
              describe: "dsa",
              id: 2,
              id_author: 1,
              image: null,
              news_user_id: {id: 1, login: "d.tkachenko@dunice.net", password: null, avatar: "https://lh3.googleusercontent.com/a-/AOh14GhfICNpuvr84UVZ78T201uVqqyUL64fxCUh5-3d=s96-c", createdAt: "2021-07-20T13:01:05.135Z" },
              tags: "dsa",
              title: "das",
              updatedAt: "2021-07-20T13:01:11.255Z",
            })
          });
        },
      };
    });
    await registration(req, res);
    expect(statusMock).toHaveBeenCalledWith(200);
  });
});
