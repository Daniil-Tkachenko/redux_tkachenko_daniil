const app = require('../app');
const request = require('supertest')

jest.mock('../queries/users.query', () => {
  return {
    getUser: ({ id }) => {
      if (isNaN(Number(id))) throw new Error(400);
      return new Promise((res, rej) => {
        res({
          login: 'xyz@abc.com',
          password: 'good',
          avatar: 'avatar',
        })
      });
    }
  }
});

describe('GET /users', () => {
  test('should response with a 200 status code', async () => {
    const response = await request(app)
    .get('/users/2')
    .send({})
    expect(response.statusCode).toEqual(200)
  });
  test('should response with defined properties', async () => {
    const response = await request(app)
    .get('/users/2')
    .send({})
    expect(response?.body?.login).toBeDefined();
    expect(response?.body?.password).toBeDefined();
    expect(response?.body?.avatar).toBeDefined();
  });
  test('should response with a 400 status code if passed wrong id', async () => {
    const response = await request(app)
    .get('/users/undefined')
    .send({})
    expect(response?.statusCode).toEqual(400);
  });
});