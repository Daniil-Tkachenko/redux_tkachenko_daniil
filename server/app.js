const multer  = require("multer");
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var app = express();

app.use(cors());

var indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const newsRouter = require('./routes/news');
const authRouter = require('./routes/auth');
const registrationRouter = require('./routes/registration');
const googleAuthRouter = require('./routes/googleAuth');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const avatarRouter = require('./controllers/avatar');
const imageRouter = require('./controllers/image');

app.use('/', indexRouter);
app.use('/news', newsRouter);
app.use('/signIn', authRouter);
app.use('/registration', registrationRouter);
app.use('/users', usersRouter);
app.use('/googleAuth', googleAuthRouter);

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, 'public/avatars');
  },
  filename: (req, file, cb) => {
      imageName = Date.now() + path.extname(file.originalname)
      cb(null, imageName); 
    }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
      cb(null, true);
  } else {
      cb(null, false);
  }
}
const upload = multer({ storage: storage, fileFilter: fileFilter });

app.post('/upload/:id', upload.single('image'), avatarRouter.putAvatar);



const storageImage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, 'public/news');
  },
  filename: (req, file, cb) => {
      imageName = Date.now() + path.extname(file.originalname)
      cb(null, imageName);
    }
});

const uploadImage = multer({ storage: storageImage, fileFilter: fileFilter });

app.post('/newsImage/:id', uploadImage.single('image'), imageRouter.uploadImage);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
