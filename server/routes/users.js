var express = require('express');
const tasksController = require('../controllers/user');

var router = express.Router();

router.post('/', tasksController.getNewsOfCertainUser);
router.put('/', tasksController.putUser);
router.get('/:id', tasksController.getUser);
router.patch('/:email', tasksController.getUserByLogin);

module.exports = router;