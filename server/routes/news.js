var express = require('express');

var router = express.Router();
const tasksController = require('../controllers/news');

router.get('/', tasksController.list);

router.post('/', tasksController.addNews);

module.exports = router;
