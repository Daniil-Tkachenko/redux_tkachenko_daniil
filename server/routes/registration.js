var express = require('express');

var router = express.Router();
const register = require('../controllers/registration')

router.post('/', register.registration);

module.exports = router;