var express = require('express');

var router = express.Router();
const upload = require('../controllers/upload')

router.post('/', upload.uploadImage);

module.exports = router;