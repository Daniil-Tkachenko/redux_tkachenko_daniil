var express = require('express');

var router = express.Router();
const avatarController = require('../controllers/avatar');

router.put('/', avatarController.putAvatar);

module.exports = router;