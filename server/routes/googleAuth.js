var express = require('express');

var router = express.Router();
const googleAuth = require('../controllers/googleAuth')

router.post('/', googleAuth.googleSignIn);
router.put('/:id', googleAuth.uploadAvatarOfGoogleUser);

module.exports = router;
