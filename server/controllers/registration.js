const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtSecretWord } = require('../config/config');

const { getUserbyLogin, createUser } = require('../queries/registration.query');

const registration = async (req, res) => {
    let { email, password } = req.body;
    password = bCrypt.hashSync(password, 10);
    return getUserbyLogin(email)
        .then((user) => {
            if (!user) {
                createUser({ email, password })
                .then((user) => {
                    const token = jwt.sign(user.id.toString(), jwtSecretWord)
                    const data = {
                        token: token,
                        id_author: user.id,
                        login: user.login,
                    }
                    res.status(200).send(JSON.stringify(data));
                })
            } else {
                res.status(401).send({ message: "Такой пользователь уже существует" });
            }
        })
        .catch(err => res.status(500).send({ message: err.message }))
}

module.exports = {
    registration,
};
