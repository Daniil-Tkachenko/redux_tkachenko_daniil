const { User } = require('../models');

const googleSignIn = (req, res) => {
    let { email } = req.body;
    return User
        .findOne({
            where: { login: email },
        })
        .then((user) => {
            if (!user) {
                User.create({
                    login: email,
                })
                .then((user) => {
                    const data = {
                        id: user.id,
                        login: user.login,
                    }
                    res.status(200).send(JSON.stringify(data));
                })
            } else {
                res.status(200).send(user);
            }
        })
        .catch(err => res.status(500).send({ message: err.message }))
}

const uploadAvatarOfGoogleUser = (req, res) => {
    const { id } = req.params;
    const { avatar } = req.body;
    return User
    .update({
        avatar: avatar
      },
      {
          where: {id: id}
      })
      .then((user) => res.status(201).send(user))
      .catch((error) => res.status(400).send(error));
}

module.exports = {
    googleSignIn,
    uploadAvatarOfGoogleUser,
};
