const { User } = require('../models');
const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtSecretWord } = require('../config/config');

const signIn = (req, res) => {
    const { email, password } = req.body;
    return User
        .findOne({ 
            where: { login: email } 
        })
        .then((user) => {
            if(!user) {
                res.status(401).send({ message: 'User does not exist'})
            }
            const isValid = bCrypt.compareSync(password, user.password)

            if (isValid) {
                const token = jwt.sign(user.id.toString(), jwtSecretWord)
                const data = {
                    token: token,
                    id_author: user.id,
                    login: user.login,
                }
                res.status(200).send(JSON.stringify(data));
            } else {
                res.status(401).send({ message: 'Invalid credentials' })
            }
        })
        .catch(err => res.status(500).send({ message: err.message }))
}

module.exports = {
    signIn,
};
