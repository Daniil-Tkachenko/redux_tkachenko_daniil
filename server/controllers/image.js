const multer  = require("multer");
const { News } = require('../models');
const { SERVER_URL } = require('../config/config')
  
  const uploadImage = (req, res) => {
    const { filename } = req.file;
    const { id } = req.params;
    return News
    .update({
        image: `${SERVER_URL}news/${filename}`
      },
      {
          where: {id: id}
      })
      .then((user) => res.status(201).send(user))
      .catch((error) => res.status(400).send(error));
};

  module.exports = {
    uploadImage,
};