const { User } = require('../models');
const { SERVER_URL } = require('../config/config')

module.exports = {
    putAvatar(req, res) {
        const { filename } = req.file;
        const { id } = req.params;
        return User
        .update({
            avatar: `${SERVER_URL}avatars/${filename}`
          },
          {
              where: {id: id}
          })
          .then((user) => res.status(201).send(user))
          .catch((error) => res.status(400).send(error));
    }
}