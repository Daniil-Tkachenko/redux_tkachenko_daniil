const { News } = require('../models');
const { User } = require('../models');

module.exports = {
  list(req, res) {
    return News
      .findAll({
        include: [{
          model: User,
          as: 'news_user_id'
        }],
        order: ['createdAt'],
      })
      .then((news) => res.status(200).send(news))
      .catch((error) => res.status(400).send(error));
  },

  addNews(req, res) {
    const { title, describe, image, id_author, tags } = req.body;
    return News
    .create({
        title: title,
        describe: describe,
        image: image,
        id_author: id_author,
        tags: tags,
      })
      .then((news) => res.status(201).send(news))
      .catch((error) => res.status(400).send(error));
  },
};
