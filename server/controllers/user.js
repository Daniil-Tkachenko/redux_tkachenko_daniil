const bCrypt = require('bcrypt');

const { User, News } = require('../models');
const { getUser } = require('../queries/users.query');

module.exports = {
    getNewsOfCertainUser(req, res) {
        const { id } = req.body;
        return News
            .findAll({
                include: [{
                model: User,
                as: 'news_user_id'
                }],
                order: ['createdAt'],
                where: { id_author: id }
            })
        .then((news) => res.status(200).send(news))
        .catch((error) => res.status(400).send(error));
    },

    putUser(req, res) {
        const { email, id_author, password } = req.body;
        return User
        .update({
            login: email,
            password: bCrypt.hashSync(password, 10),
          },
          {
              where: {id: id_author}
          })
          .then((user) => res.status(201).send(user))
          .catch((error) => res.status(400).send(error));
    },

    async getUser(req, res) {
        try {
            const { id } = req.params;
            const user = await getUser({ id });
            return res.status(200).send(user)
        } catch (error) {
            return res.status(400).send(error)
        }
    },

    getUserByLogin(req, res) {
        const { email } = req.params;
        return User
            .findOne({
            where: { login: email }
            })
            .then((user) => res.status(200).send(user))
            .catch((error) => res.status(400).send(error));
    },
}