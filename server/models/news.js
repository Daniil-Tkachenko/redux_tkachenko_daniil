'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class News extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      News.belongsTo(models.User, {
        foreignKey: 'id_author',
        as: 'news_user_id'
      });
    }
  };
  News.init({
    title: DataTypes.TEXT,
    describe: DataTypes.TEXT,
    image: DataTypes.TEXT,
    id_author: DataTypes.INTEGER,
    tags: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'News',
  });
  return News;
};

